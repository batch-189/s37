const express = require('express')
const mongoose = require('mongoose')
const dotenv = require('dotenv')

const cors = require('cors')
const port = 4000

dotenv.config()

const app = express();


app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended: true}));

mongoose.connect(`mongodb+srv://admin12345:${process.env.MONGODB_PASSWORD}@zuitt-bootcamp.qquvkqu.mongodb.net/activity/s37-s41?retryWrites=true&w=majority`,{
			useNewUrlParser: true,
			useUnifiedTopology: true
})

let db = mongoose.connection
db.on('error', () => console.error.bind(console, 'error'))
db.once(`open`, () => console.log(`Now connected in mongoDB Atlas`))


app.listen(process.env.PORT || port,() => {
	console.log(`API is now online on port ${process.env.PORT || port}`)
})


